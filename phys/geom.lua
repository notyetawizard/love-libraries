--------------------------------------------------------------------------------
-- Secret data type verifications
--------------------------------------------------------------------------------
-- These are a total waste of resources if called often and on things
-- which have already been verified. So don't do that.

local function verifyPoint(point, name)
	assert(
		type(p) == "table",
		"The point "..(name or "passed").." is not a valid point ({x, y})."
	)
	assert(
		type(p[1]) == "number",
		"The x position of the point "..(name or "passed").." is not a valid number."
	)
	assert(
		type(p[2]) == "number",
		"The y position of the point "..(name or "passed").." is not a valid number."
	) 
end
	
local function verifyPointGroup(pointGroup, allowDuplicates)
	assert(
		type(pointGroup) == "table",
		"The function phys.newShape requires an array of points ({x, y}) as it's first argument."
	)
	for i, p in ipairs(pointGroup) do
		verifyPoint(p, "at index "..i)
	end
end


--------------------------------------------------------------------------------
-- The actual for realsies library!
--------------------------------------------------------------------------------
local geom = {}

function geom.eliminateDuplicatePoints(pointGroup)
	local checkme = {}
	local duplicates = {}
	for i, pa in ipairs(pointGroup) do
		local dupe = false
		for _, pb in ipairs(checkme) do
			if pb[1] == pa[1] and pb[2] == pb[2] then
				table.insert(duplicates, i)
				dupe = true
			end
		end
		if dupe == false then
			table.insert(checkme, pa)
		end
	end
	
	for _, i in ipairs(duplicates) do
		table.remove(pointGroup, i)
	end
	
	return pointGroup
end

function geom.eliminateAlignedPoints(pointGroup)
	-- What about perfectly aligned points on a spoke? I think we just kill off the inners?
	-- Otherwise there's concaveness and difficulty with the implicit ordering of points

	return pointGroup
end

function geom.getCentroid(pointGroup)
	verifyPointGroup(pointGroup)
	
	local totalp = 0
	local totalx = 0
	local totaly = 0
	for _, p in ipairs(pointGroup) do
		totalp = totalp + 1
		totalx = totalx + p1
		totaly = totaly + p2
	end
	
	return {totalx/totalp, totaly/totalp}
end

function geom.zeroCentroid(pointGroup)
	verifyPointGroup(pointGroup)

	local offset = geom.getCentroid(pointGroup)
	for _, p in ipairs(pointGroup) do
		p[1] = p[1] - offset[1]
		p[2] = p[2] - offset[2]
	end

	return pointGroup
end

function geom.getBoundingBox(pointGroup, centroid)
	points = verifyPointGroup(pointGroup)

	local minx = centroid[1]
	local maxx = centroid[1]
	local miny = centroid[2]
	local maxy = centroid[2]
	for _, p in ipairs(points) do
		if p[1] < minx then
			minx = p[1]
		elseif p[1] > maxx then
			maxx = p[1]
		end
		if p[2] < miny then
			miny = p[2]
		elseif p[2] > maxy then
			maxy = p[2]
		end
	end

	return {{minx, miny}, {maxx, maxy}}
end


function geom.orderPointGroup(pointGroup)
	local points = {}

end

--------------------------------------------------------------------------------
-- Create a new shape, with custom points!
--------------------------------------------------------------------------------
-- pointGroup:
-- A group of points, like so: {{0, 0}, {0, 1}, {1, 1}, {1, 0}}
--------------------------------------------------------------------------------
-- axisPoint:
-- The point the shape will rotate around. Put it inside the shape or expect weird.
-- Or nothing! Will default to the center of the shape if not given.
--------------------------------------------------------------------------------
function geom.newShape(pointGroup)
	-- Verify that we got good arguments
	verifyPointGroup(pointGroup)

	-- Get cool and useful things from the point group
	pointGroup = geom.eliminateDuplicatePoints(pointGroup)
	pointGroup = geom.eliminateAlignedPoints(pointGroup)
	pointGroup = geom.zeroCentroid(pointGroup)



	-- Area!
	-- Mass Multiplier!

	-- Don't put things in here until you're sure you want to pass them on.
	-- Do everything locally until you're really sure it needs to be made public!
	local shape = {}

	return shape
end


-- Shape collision:
-- Check if within outer bounding
-- Check if within inner bounding! (could be missed by next step, and is quicker)
-- Oh, actually this is super unlikely! I forgot about the inner spokes. It'd be more
-- Likely for an object to miss a collison near the edge than the centre.
-- Then per spoke of radial shape ... in order of probable closeness?
-- All Edges then all spokes? Directional?

return geom