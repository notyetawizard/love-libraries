### Now

- Create objects of various shapes in a lovely way.
- Find the best math to do it with.
- Collision checks!


### Later

- Mass
- Force
- Momentum
- Objects are sorted on a grid, to minimize need for collision checks.
- Shape groups, for complex/concave polygons and such!

